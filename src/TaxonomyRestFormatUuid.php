<?php

namespace Drupal\restful_tools;

/**
 * Convert terms into rest format.
 */
class TaxonomyRestFormatUuid {

  /**
   * Convert term to taxonomy.
   *
   * @param mixed $value
   *   Term loaded, or id.
   */
  public static function format($value) {
    $term = !is_object($value) ? taxonomy_term_load($value) : $value;
    return array(
      'uuid'  => $term->uuid,
      'name'  => $term->name,
    );
  }

}
