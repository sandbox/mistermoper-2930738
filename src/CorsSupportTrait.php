<?php

namespace Drupal\restful_tools;

/**
 * Give simple cors support to a basic endpoint.
 */
trait CorsSupportTrait {

  /**
   * {@inheritdoc}
   */
  public function discover($path = NULL) {
    $this->preflight($path);
    // Don't show for anonymous!
    if (user_is_anonymous()) {
      $result = array();
    }
    else {
      $result = parent::discover($path = NULL);
    }
    return $result;
  }

}
