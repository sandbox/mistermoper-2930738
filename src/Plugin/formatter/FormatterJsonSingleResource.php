<?php

namespace Drupal\restful_tools\Plugin\formatter;

use Drupal\restful\Plugin\formatter\FormatterJson as FormatterJsonBase;

/**
 * Class FormatterJsonSingleResource.
 *
 * Return only first result of a resource request. Use this if you
 * know this resource will return just one value.
 *
 * If there is more than one value it returns all.
 *
 * @NOTE: check a way to integrate with restful.
 *
 * @package Drupal\restful_tools\Plugin\formatter
 *
 * @Formatter(
 *   id = "json_single_resource",
 *   label = "JSON single resource",
 *   description = "Output in using the JSON format, allowing retrieve one value for single resource."
 * )
 */
class FormatterJsonSingleResource extends FormatterJsonBase {

  /**
   * {@inheritdoc}
   */
  public function limitFields($output, $allowed_fields = NULL) {
    $return = parent::limitFields($output, $allowed_fields);
    return !isset($allowed_fields) && count($output) == 1 ? reset($return) : $return;
  }

}
