<?php

namespace Drupal\restful_tools;

/**
 * Decorate roles property in restfull callbacks.
 */
class RolesDecorator {

  /**
   * Process user role list.
   *
   * Structure
   * [
   *   {
   *     rid: [rid],
   *     name: [name]
   *   }
   * ]
   *
   * @return array
   *   Roles formatted.
   */
  public static function format($roles) {
    $user_roles = [];
    foreach ($roles as $rid) {
      $role = user_role_load($rid);
      $user_roles[] = array(
        'rid' => $role->rid,
        'name' => $role->name,
      );
    }
    return $user_roles;
  }

  /**
   * Process user role list.
   *
   * @return array
   *   Roles formatted.
   */
  public static function formatPlain($roles) {
    $user_roles = [];
    foreach ($roles as $rid) {
      $role = user_role_load($rid);
      $user_roles[] = $role->name;
    }
    return $user_roles;
  }
}
