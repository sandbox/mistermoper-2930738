<?php

namespace Drupal\restful_tools;

use Drupal\restful\Plugin\resource\DataInterpreter\DataInterpreterEMW;

/**
 * Get user status in a restful public field for a users endpoint.
 */
class UserResourceStatus {

  /**
   * Get user status from wrapper.
   *
   * @param \Drupal\restful\Plugin\resource\DataInterpreter\DataInterpreterEMW $interpreter
   *   Data interpreter.
   *
   * @return int
   *   User status.
   */
  public static function get(DataInterpreterEMW $interpreter) {
    return $interpreter->getWrapper()->status->value();
  }

}
